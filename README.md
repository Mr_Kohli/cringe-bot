# Cringe Bot

A Discord Bot, that every time someone sends "Cringe" in a Text-Channel, a video is sent.



## How to use
Before using the bot you need to set it up and configure it, please see the [Setup](#setup) and [Configuration](#configuration)
sections.


In order to start up the bot, you can just run the following command in the console:

```commandline
python main.py
```



## Setup
Before running the bot, you need to create a config.yaml file in the data directory. 

Additionally, you need to get a Discord-Bot Token. An instruction on how to do that can be found [here](https://www.digitaltrends.com/gaming/how-to-make-a-discord-bot/)
(Steps 2-4).




Before running the bot for the first time, you need to install the requirements using the following command:

```commandline
pip install -r requirements.txt
```



## Configuration
### Discord
| Option |      Description      | Default |
|:------:|:---------------------:|---------|
| token* | The Discord Bot Token | None    |

### Reddit
|     Option     |             Description              | Default |
|:--------------:|:------------------------------------:|---------|
|     enable     | If you want to enable the Reddit API | false   |


*required for the bot to function

### Example configuration
```yaml
discord:
  token: "<Discord Bot Toke>"

reddit:
  enable: true
```







