import logging
import random
import requests
import urllib.request

from random import randint
from bs4 import BeautifulSoup

from modules import colors


def load_subs():
    # Read subreddit list
    url = 'https://gitlab.com/Mr_Kohli/cringe-bot-subreddits/-/raw/main/subs.csv'
    response = urllib.request.urlopen(url)
    subs = [l.decode('utf-8') for l in response.readlines()]

    # Remove the "\n"
    for i in range(len(subs)):
        subs[i] = subs[i].replace("\n", "")

    return subs


def get_content(souped_page, user_id):
    content_link = None
    div_id = "thing_t3_" + user_id

    # Get content
    for post in souped_page.find_all('div', id=div_id):
        post_url = post.attrs['data-url']
        content_link = post_url

    if content_link is None:
        div_id = "video-" + user_id

        # Get content
        for post in souped_page.find_all('div', id=div_id):
            post_url = post.attrs['data-url']
            content_link = post_url

    return content_link


def get_webpage(link):
    # Request headers
    web_header = {'User-Agent': 'Mozilla/5.0'}

    # Request webpage
    page = requests.get(link, headers=web_header)

    return page


def get_post(souped_page, sub):
    post_links = []

    # Get Reddit content
    attrs = {'class': 'thing'}
    for post in souped_page.find_all('div', attrs=attrs):
        post_url = post.attrs['data-permalink']
        post_data_type = post.attrs['data-domain']

        if "self" not in post_data_type:
            post_links.append(post_url)
        else:
            logging.info('Text post, skipping')

    return post_links


def get_usr_id(post_link, sub):
    temp = post_link.replace(sub, '')
    temp = temp.replace('//comments/', '')
    usr_id = temp.split('/')[0]
    return usr_id


def request_img():
    # Loop until an existing sub is found
    failed = True
    souped_page = None

    while failed:
        # Get and select the Subreddit
        sub_list = load_subs()
        rand = randint(0, (len(sub_list) - 1))
        sub = sub_list[rand]

        # Pull the website
        reddit_url = "https://old.reddit.com/" + sub
        page = get_webpage(reddit_url)
        souped_page = BeautifulSoup(page.text, 'html.parser')

        # Filter out banned/not existing subs
        if page.status_code == 404:
            logging.error('The following Sub does not exists: ' + sub + 'Please remove it from the list')
            failed = True
        elif "subreddits" in page.url:
            logging.error('The following Sub does not exists: ' + sub + 'Please remove it from the list')
            failed = True
        else:
            failed = False

    # Get and select a post
    post_list = get_post(souped_page, sub)
    rand_post = randint(0, (len(post_list) - 1))
    random.shuffle(post_list)
    post_link = post_list[rand_post]

    # Get content page
    reddit_url = "https://old.reddit.com/" + post_link
    page = get_webpage(reddit_url)
    souped_page = BeautifulSoup(page.text, 'html.parser')

    # get user ID
    usr_id = get_usr_id(post_link, sub)

    # get content link
    content_link = get_content(souped_page, usr_id)

    return [content_link, sub, post_link]
