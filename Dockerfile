FROM python:3.12-slim-bookworm

WORKDIR /usr/app/src

COPY main.py ./
COPY data ./data
COPY modules ./modules
COPY requirements.txt ./

RUN ["pip", "install", "-r", "requirements.txt"]
CMD [ "python", "./main.py", "-d" ]
