# Necessary python packages
try:
    import discord
    import os
    import platform
    import yaml
    import argparse
    import logging
    from random import randint
    from yaml.loader import SafeLoader

except ModuleNotFoundError as e:
    # ANSI Stuff
    PREF = "\033["
    RESET = f"{PREF}0m"
    ERROR = "31m"
    WARNING = "33m"

    logging.error(f'{PREF}0;{ERROR}' + 'Alert: ' + RESET + 'a required package is not installed!')
    quit(-1)


# Custom Modules
try:
    from modules import reddit
    from modules import colors
    from modules import utils

except ModuleNotFoundError as e:
    PREF = "\033["
    RESET = f"{PREF}0m"
    ERROR = "31m"
    WARNING = "33m"
    logging.error(e)
    logging.error('Missing Module! Please download it from the following gitlab page: '
                  'https://gitlab.com/Mr_Kohli/cringe-bot')

    quit(-1)


# Discord Client
intents = discord.Intents.all()
intents.typing = True
intents.presences = True
client = discord.Client(intents=intents)

config = [
    None,       # Discord Token
    False       # Reddit
]


def initializer():
    # --- Parse Flags ---
    parser = argparse.ArgumentParser()
    # Add arguments
    parser.add_argument('--TOKEN')
    parser.add_argument('-d', action='store_true')
    # parse the arguments
    args = parser.parse_args()
    # Init logging
    logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s: %(message)s')

    # === Docker Mode ===
    if args.d:
        try:
            config[0] = os.environ.get('DC_TOKEN')
            config[1] = bool(utils.strtobool(os.environ.get('RE_ENABLE')))
        except AttributeError as err:
            logging.error('Invalid environment variables. Please see the README for help')
            quit(-1)
        if config[0] is None or config[0] == "":
            logging.error('No token defined! Please see the README for help')
            quit(-1)
        client.run(str(config[0]))

    # === Script Mode ===
    if not os.path.isfile('data/config.yaml'):
        logging.error('config.yaml is missing! Please see the README for help')
        quit(-1)

    with open("data/config.yaml", 'r') as f:
        data = yaml.load(f, Loader=SafeLoader)

    # -- Get discord token ---
    # Check if the token is set in the arguments and use it
    if args.TOKEN is not None:
        config[0] = args.TOKEN
    else:
        # Get discord Token
        config[0] = None
        try:
            config[0] = data['discord']['token']
        except KeyError as err:
            logging.error('config.yaml not formatted right! Please see the README for help')
            quit(-1)
        if config[0] is None or config[0] == "":
            logging.error('Discord Bot Token is missing! Please see the README for help')
            quit(-1)

    # --- get Reddit API config ---
    try:
        config[1] = bool(data['reddit']['enable'])
    except KeyError as err:
        logging.info('Invalid Reddit API configuration, disabling it as default')

    # Start bot
    client.run(str(config[0]))


def helper():
    # Set the header
    embed = discord.Embed(title="Cringe-Bot:tm: Manual",
                          description="The best bot to have ever been created by anyone.*\n\n"
                                      "This bot sends a video every time someone says *the word* a video.\n"
                                      "It also offers you the following command:"
                                      "```get-cringe```",
                          color=discord.Color.blue())
    # Set the footer
    embed.set_footer(text="*If you check that, you're big cringé!")

    if config[1]:
        # get-cringe command
        embed.add_field(name="get-cringe",
                        value="Sends a very cringé meme from reddit")

    return embed


async def message_handler(message, response):
    # finds right response
    match response:
        # get-cringe
        case "getCringe":
            if config[1]:
                # Send status message due to long delay
                msg = "Getting some cringe from Reddit"
                await message.channel.send(msg)

                # Get cringe post link
                returns = reddit.request_img()

                msg = "Sending [cringe](<https://reddit.com" + returns[2] + ">) from: " + returns[1]
                await message.channel.send(msg)

                # Send cringe
                msg = returns[0]
                await message.channel.send(msg)

                # Write Protocol
                logging.info(f'{colors.PREF}0;{colors.USER}' + str(message.author) + colors.RESET +
                             " wanted to see the following cringe: " + f'{colors.PREF}0;{colors.MESSAGE}' +
                             msg + colors.RESET)
            else:
                await message.channel.send("Reddit is not enabled")

        # get-help
        case "help":
            # Generating help message
            embed = helper()
            await message.channel.send(embed=embed)

            # Write Protocol
            logging.info(f'{colors.PREF}0;{colors.USER}' + str(message.author) + colors.RESET + " needs some help!")

        # cringe person
        case "cringe":
            # Open Video file
            f = open("data/cringe.mp4", "rb")
            # Convert video to a message
            video = discord.File(f)

            await message.channel.send('Oh no cringé!')
            await message.channel.send(file=video)
            logging.info(f'{colors.PREF}0;{colors.USER}' + str(message.author) + colors.RESET + " is very cringe!")


def message_interpreter(message):
    # cleans message
    message_send = message.content.lower()
    message_send = message_send.split(' ')

    if message.author == client.user:
        return "stop"

    if any('get-cringe' in s for s in message_send):
        return "getCringe"

    if any('help-cringe' in s for s in message_send):
        return "help"

    if any('cringe' in s for s in message_send):
        return "cringe"

    if any('cringé' in s for s in message_send):
        return "cringe"

    if any('cr¡nge' in s for s in message_send):
        return "cringe"


@client.event
async def on_ready():
    utils.clear_console()
    logo1 = "   _____ _____  _____ _   _  _____ ______   ____   ____ _______ \n"
    logo2 = "  / ____|  __ \|_   _| \ | |/ ____|  ____| |  _ \ / __ \__   __|\n"
    logo3 = " | |    | |__) | | | |  \| | |  __| |__    | |_) | |  | | | |   \n"
    logo4 = " | |    |  _  /  | | | . ` | | |_ |  __|   |  _ <| |  | | | |   \n"
    logo5 = " | |____| | \ \ _| |_| |\  | |__| | |____  | |_) | |__| | | |   \n"
    logo6 = "  \_____|_|  \_\_____|_| \_|\_____|______| |____/ \____/  |_|   \n"
    logging.info(f'{colors.PREF}0;{colors.LOGO}' + "\n" + logo1 + logo2 + logo3 + logo4 + logo5 + logo6 + colors.RESET)
    logging.info('Cringe Bot is now' + f'{colors.PREF}0;{colors.GREEN}' + ' online' + colors.RESET + ' as' +
                 f'{colors.PREF}0;{colors.USER}' + ' {0.user}'.format(client) + colors.RESET)


@client.event
async def on_message_edit(before, message):
    response = message_interpreter(message)
    if response == "stop":
        return
    await message_handler(message, response)


@client.event
async def on_message(message):
    if message.author == client.user:
        return

    response = message_interpreter(message)
    await message_handler(message, response)


if __name__ == '__main__':
    initializer()

